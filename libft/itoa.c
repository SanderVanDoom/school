/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/13 18:42:25 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/13 18:42:25 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

char	*ft_itoa(int n)
{
	char	*s;
	size_t	len;
	int		tmp;

	tmp = n;
	len = 0;
	if (n < 0)
	{
		len = 1;
		n *= -1;
	}
	while (++len && tmp)
		tmp /= 10;
	s = (char *) malloc(len * sizeof(char));
	if (!s)
		return (0);
	s[--len] = '\0';
	while (n)
	{
		s[--len] = '0' + (n % 10);
		n /= 10;
	}
	if (len > 0)
		s[--len] = '-';
	return (s);
}
