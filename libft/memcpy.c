/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memcpy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/05 09:01:24 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 11:08:00 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

void	*ft_memcpy(void *restrict des, const void *restrict src, size_t s)
{
	const char		*ps;
	char			*pd;
	size_t			c;

	ps = src;
	pd = des;
	c = 0;
	while (c++ < s)
	{
		*pd = *ps;
		pd++;
		ps++;
	}
	return (des);
}
