/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/15 01:08:06 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/15 01:08:06 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef LIBFT_H
# define LIBFT_H

# include <stdlib.h>
# include <unistd.h>

int		ft_atoi(const char *s);
char	*ft_itoa(int n);
void	ft_bzero(void *p, size_t s);
void	*ft_calloc(size_t c, size_t s);
int		ft_isalnum(int c);
int		ft_isalpha(int c);
int		ft_isascii(int i);
int		ft_isdigit(int c);
int		ft_isprint(int i);
void	*ft_memcpy(void *restrict des, const void *restrict src, size_t s);
void	*ft_memmove(void *dst, const void *src, size_t l);
void	*ft_memset(void *p, int v, size_t l);
char	*ft_strchr(const char *s, int c);
char	*ft_strdup(const char *s1);
size_t	ft_strlcat(char *restrict des, char *restrict src, size_t s);
size_t	ft_strlcpy(char *restrict des, char *restrict src, size_t s);
int		ft_strlen(const char *s);
char	*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int		ft_strncmp(const char *s1, const char *s2, size_t n);
char	*ft_strnstr(const char *b, const char *l, size_t n);
char	*ft_strrchr(const char *s, int c);
char	**ft_strsplit(char *s, char d);
char	*ft_substr(char const *s, unsigned int start, size_t len);
int		ft_tolower(int c);
int		ft_toupper(int c);
void	ft_striteri(char *s, void (*f)(unsigned int, char*));
void	ft_putchar_fd(char c, int fd);
void	ft_putstr_fd(char *s, int fd);
void	ft_putendl_fd(char *s, int fd);
void	ft_putnbr_fd(int n, int fd);

#endif