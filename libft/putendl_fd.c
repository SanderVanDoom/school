/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putendl_fd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/15 03:22:53 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/15 03:22:53 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

void	ft_putendl_fd(char *s, int fd)
{
	write(fd, &s, ft_strlen(s));
	ft_putchar_fd('\n', fd);
}
