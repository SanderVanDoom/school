/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strtrim.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/13 01:02:28 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 14:13:04 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

char	*ft_strtrim(const char *s, const char *c)
{
	char		*p;
	size_t		len;

	len = ft_strlen(s);
	p = malloc(len * sizeof(char));
	if (!p)
		return (NULL);
	while (*s == *(c++))
		s++;
	
	p -= len;
	return (p);
}
