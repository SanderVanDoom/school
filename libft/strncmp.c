/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strncmp.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 08:51:21 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/08 14:49:44 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	c1;
	unsigned char	c2;

	while (n-- > 0 && (*s1 || *s2))
	{
		if (*s1 != *s2)
		{
			c1 = *s1;
			c2 = *s2;
			return (c1 - c2);
		}
		s1++;
		s2++;
	}
	return (0);
}
