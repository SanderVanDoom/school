/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   substr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 15:06:20 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 10:01:19 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*ss;
	int		c;
	char	cc;

	c = 0;
	cc = start;
	ss = (char *) malloc((len + 1) * sizeof(char));
	while (*s)
	{
		if (*s == cc)
		{
			while (*s && len--)
			{
				*ss = *s;
				ss++;
				s++;
				c++;
			}
			*ss = 0;
			ss -= c;
			return (ss);
		}
		s++;
	}
	return (0);
}
