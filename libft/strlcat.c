/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlcat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 14:39:45 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 09:56:13 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

size_t	ft_strlcat(char *restrict des, char *restrict src, size_t s)
{
	size_t	c;

	c = 0;
	while (*des != '\0')
	{
		des++;
	}
	while (*src != '\0' && c < s)
	{
		*des = *src;
		des++;
		src++;
		c++;
	}
	*des = '\0';
	return (c);
}
