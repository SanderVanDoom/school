/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strjoin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 15:27:00 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/08 15:41:28 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*ct;
	int		len1;
	int		len2;

	len1 = 0;
	len2 = 0;
	while (s1[len1])
		len1++;
	while (s2[len2])
		len2++;
	ct = (char *) malloc((len1 + len2) * sizeof(char));
	if (!ct)
		return (0);
	ct[len1 + len2] = 0;
	while (*s1)
		*(ct++) = *(s1++);
	while (*s2)
		*(ct++) = *(s2++);
	ct -= len1 + len2;
	return (ct);
}
