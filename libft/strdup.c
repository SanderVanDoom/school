/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 12:31:55 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/08 13:08:53 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

char	*ft_strdup(const char	*s1)
{
	char	*p;
	int		len;
	int		m;

	len = 0;
	while (*s1 != '\0')
	{
		len++;
		s1++;
	}
	p = (char *) malloc((len + 1) * sizeof(char));
	s1 -= len;
	m = len;
	while (*s1 != '\0')
	{
		*p = *s1;
		p++;
		s1++;
	}
	*p = '\0';
	p -= m;
	return (p);
}
