/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strnstr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 09:52:51 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/08 14:52:48 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

char	*ft_strnstr(const char *b, const char *l, size_t n)
{
	int	c;

	c = 0;
	if (!*l)
		return ((char *)b);
	while (n-- > 0 && *b != '\0')
	{
		while (*b == *l)
		{
			if (*(l + 1) == '\0')
				return ((char *) b);
			b++;
			l++;
			c++;
		}
		l -= c;
		b++;
	}
	return (0);
}
