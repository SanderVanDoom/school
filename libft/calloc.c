/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/08 10:43:05 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 13:56:15 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

void	*ft_calloc(size_t c, size_t s)
{
	int		*p;
	size_t	i;

	p = malloc(c * s);
	if (!p)
		ft_bzero(p, c);
	return (p);
}
