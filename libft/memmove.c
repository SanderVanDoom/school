/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memmove.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/05 10:06:30 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 09:53:25 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

void	*ft_memmove(void *dst, const void *src, size_t l)
{
	void		*p;
	int			c;

	p = dst;
	c = l;
	while (*(int *)src && l-- > 0)
	{
		*(int *)dst = *(int *)src;
		src++;
		dst++;
	}
	*(int *)dst = 0;
	dst -= c;
	return (p);
}
