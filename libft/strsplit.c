/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strsplit.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/13 08:29:50 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/13 08:29:50 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include"libft.h"

char	**ft_strsplit(char *s, char d)
{
	size_t	len;
	size_t	i;
	char	**r;

	len = ft_strlen(s);
	i = 0;
	r = malloc((len + 2) * sizeof(char *));
	if (!r)
		return (0);
	*(r + len + 1) = 0;
	*r = s;
	while (*s)
	{
		if (*s == d)
		{
			*s = '\0';
			*(++r) = ++s;
		}
		s++;
	}
	r -= len;
	return (r);
}
