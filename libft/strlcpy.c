/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlcpy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oshimi <shimi_omar@hotmail.com>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 13:16:00 by oshimi            #+#    #+#             */
/*   Updated: 2021/11/16 09:58:07 by oshimi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"libft.h"

size_t	ft_strlcpy(char *restrict des, char *restrict src, size_t s)
{
	size_t	c;

	c = 0;
	while (*src != '\0' && c < s)
	{
		*des = *src;
		des++;
		src++;
		c++;
	}
	*des = '\0';
	return (c);
}
